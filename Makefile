all: test typecheck style flake8

export PROJECT := django-sync-github-teams

include $(shell tuxpkg get-makefile)
